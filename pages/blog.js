import React from "react";

import MainLayout from "../src/MainLayout";

import Blog from "../src/components/page.blog/Blog";

export default props => {
  return (
    <MainLayout>
      <Blog />
    </MainLayout>
  );
};
