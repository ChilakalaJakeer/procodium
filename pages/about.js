import React from "react";

import MainLayout from "../src/MainLayout";

import About from "../src/components/page.about/About";

export default props => {
  return (
    <MainLayout>
      <About />
    </MainLayout>
  );
};
