import React from "react";

import MainLayout from "../src/MainLayout";

import Contact from "../src/components/page.contact/Contact";

export default props => {
  return (
    <MainLayout>
      <Contact />
    </MainLayout>
  );
};
