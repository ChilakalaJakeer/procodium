import React from "react";

import MainLayout from "../src/MainLayout";

import Profile from "../src/components/page.profile/Profile";

export default props => {
  return (
    <MainLayout>
      <Profile />
    </MainLayout>
  );
};
