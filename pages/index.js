import React from "react";

import MainLayout from "../src/MainLayout";

import Home from "../src/components/page.home/Home";

export default function Index(props) {
  return (
    <MainLayout>
      <Home />
    </MainLayout>
  );
}
