import { createMuiTheme } from "@material-ui/core/styles";
import React from "react";
import {
  useMediaQuery,
  StylesProvider,
  ThemeProvider as MuiThemeProvider
} from "@material-ui/core";
import { PRIMARY, SECONDARY, PAPER, DEFAULT_BACKGROUND } from "./globalStyles";
import { ThemeProvider } from "styled-components";

/*
@pallet
      #6b96d9
      #60789d ->primary

      #d279a4
      #c7458c -> secondary

*/
export default function MainTheme(props) {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const theme = React.useMemo(
    () =>
      createMuiTheme({
        mixins: {
          toolbar: {
            minHeight: 50
          }
        },
        palette: {
          type: prefersDarkMode ? "dark" : "light",
          primary: {
            main: PRIMARY
          },
          secondary: {
            main: SECONDARY
          },
          // background: {
          //   paper: PAPER,
          //   default: DEFAULT_BACKGROUND
          // },
          // Used by `getContrastText()` to maximize the contrast between
          // the background and the text.
          contrastThreshold: 3,
          tonalOffset: 0.2
        }
      }),
    [prefersDarkMode]
  );
  return (
    <StylesProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
      </MuiThemeProvider>
    </StylesProvider>
  );
}
