// from http://stripe.com

export const GRADIENT = `linear-gradient(150deg,#53f 15%,#05d5ff 74%)`;

export const GRADIENT_INTRO = `linear-gradient(150deg,#53f 15%,#05d5ff 74%,#a6ffcb 94%)`;

//portfolio

export const HEADER_PADDING_BOTTOM = "25px"; //EQUAL_SIDE_LENTH / 6

//Logo holder dimensions

export const EQUAL_SIDE_LENTH = "150px"; //square

export const MARGIN = "-75px"; //-EQUAL_SIDE_LENTH / 2

//MainTheme consts

//mixins

export const APPBAR_HEIGHT = "px";

//pallet
export const PRIMARY = "#6b96d9";

export const SECONDARY = "#d279a4";

export const SUCCESS = "#";

export const ERROR = "#";

export const WARNING = "#";

export const INFO = "#";

export const PAPER = "#fff";

export const DEFAULT_BACKGROUND = "#fff";
