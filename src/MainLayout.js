import React from "react";

import CssBaseline from "@material-ui/core/CssBaseline";

import MainTheme from "./theme/MainTheme";
import NavBar from "./components/navBar/NavBar";
import Footer from "./components/footer/Footer";

function MainLayout(props) {
  return (
    <MainTheme>
      <CssBaseline />
      <NavBar />
      {props.children}
      <Footer />
    </MainTheme>
  );
}

export default MainLayout;
