import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/core/styles";

import { GRADIENT_INTRO } from "../../theme/globalStyles";
import { Typography } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",

    background: GRADIENT_INTRO,
    overflow: "hidden",
    transformOrigin: 0
  },
  container: {
    marginTop: theme.spacing(10),
    padding: theme.spacing(20),
    display: "flex",
    flex: 1,
    placeContent: "center"
  },
  content: {
    flex: 0.5
  }
}));

export default function Intro(props) {
  const classes = useStyles();
  const [windowHeight, setWindowHeight] = useState(0);

  useEffect(() => {
    setWindowHeight(window.innerHeight);

    return () => {
      setWindowHeight(0);
    };
  });
  console.log(windowHeight);
  return (
    <Typography
      component="div"
      className={classes.root}
      style={{ height: windowHeight }}
      ariaHidden="true"
    >
      <Typography component="div" className={classes.container}>
        <Typography component="p" className={classes.content}>
          Labore cupidatat laborum in amet fugiat magna ut. Deserunt laboris
          cupidatat ullamco elit consequat ea nulla fugiat pariatur laboris
          occaecat do. Minim ut ad enim aute et in non non ad dolore velit
          incididunt. Sint enim Lorem sit occaecat culpa do duis magna laboris
          proident. Irure et aliqua anim pariatur.
        </Typography>
      </Typography>
    </Typography>
  );
}
