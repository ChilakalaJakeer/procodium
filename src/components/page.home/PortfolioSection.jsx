import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import {
  EQUAL_SIDE_LENTH,
  HEADER_PADDING_BOTTOM
} from "../../theme/globalStyles";
import { LogoHolder, HexagonLogoHolder } from "../elements/StyledComponents";
import { Typography, Button } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridTemplateColumns: " 1fr 2fr 1fr",
    gridTemplateRows: "auto",
    gridTemplateAreas: `".       react    ."
    ".    node      ."
    ".     js      ."
    ".    mongoDB      ."
    ".      css      ."
    ".    html   ."
    ".      chatBots      ."    
    `,
    //smBelow
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: " 1fr 6fr 1fr",
      gridTemplateRows: "auto",
      gridTemplateAreas: `".       react    ."
  ".    node      ."
  ".     js      ."
  ".    mongoDB      ."
  ".      css      ."
  ".    html   ."
  ".      chatBots      ."    
  `
    },
    //xsBelow
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "0.5fr 11fr 0.5fr",
      gridTemplateRows: "auto",
      gridTemplateAreas: `".       react    ."
  ".    node      ."
  ".     js      ."
  ".    mongoDB      ."
  ".      css      ."
  ".    html   ."
  ".      chatBots      ."    
  `
    },

    //each card
    "&>*": {
      // marginBlock: "100px", // (2/3)X EQUAL_SIDE_LENTH  ->Bug with marginBlock  style not reflecting
      marginTop: EQUAL_SIDE_LENTH,
      boxShadow: theme.shadows[3],
      borderRadius: theme.spacing(1),
      background: theme.palette.background.paper,

      display: "grid",
      gridTemplateColumns: "50px auto 50px",
      gridTemplateRows: `${EQUAL_SIDE_LENTH} auto ${EQUAL_SIDE_LENTH}`,
      gridTemplateAreas: `
      
      ".     header       ."
      ".     summary       ."
      ".     footer       ."
      `,
      "&>h2": {
        gridArea: "header",
        placeSelf: "end center",
        paddingBottom: HEADER_PADDING_BOTTOM //  EQUAL_SIDE_LENTH/6
      },
      "&>p": {
        gridArea: "summary",
        placeSelf: "center"
      },
      "&>footer": {
        gridArea: "footer",
        placeSelf: "center",

        //flex
        display: "flex",
        placeContent: "center",
        flexWrap: "wrap"
      }
    }
  },

  react: {
    gridArea: "react",
    placeSelf: "center"
  },
  node: {
    gridArea: "node",
    placeSelf: "center"
  },
  js: {
    gridArea: "js",
    placeSelf: "center"
  },
  mongoDB: {
    gridArea: "mongoDB",
    placeSelf: "center"
  },
  css: {
    gridArea: "css",
    placeSelf: "center"
  },
  html: {
    gridArea: "html",
    placeSelf: "center"
  },
  chatBots: {
    gridArea: "chatBots",
    placeSelf: "center"
  }
}));

export default function PortfolioSection(props) {
  const classes = useStyles();
  return (
    <Typography component="section" className={classes.root}>
      <Typography component="div" className={classes.react}>
        <LogoHolder borderRadius="75px" />
        <Typography component="h2" variant="h5">
          ReactJS
        </Typography>
        <Typography component="p" variant="body1">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>

        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.node}>
        <HexagonLogoHolder>
          <Typography component="div" class="hexagon">
            <span></span>
          </Typography>
        </HexagonLogoHolder>
        <Typography component="h2"> NodeJS</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.js}>
        <LogoHolder />
        <Typography component="h2"> JavaScript</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.mongoDB}>
        <LogoHolder
          transform="rotate(0.125turn)"
          borderRadius="0px 300px 75px 300px" // 0 2xEQUAL_SIDE_LENTH 2xEQUAL_SIDE_LENTH EQUAL_SIDE_LENTH/2
        />
        <Typography component="h2"> MongoDB</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.css}>
        <LogoHolder borderRadius="75px" />

        <Typography component="h2"> CSS3</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.html}>
        <LogoHolder borderRadius="75px" />

        <Typography component="h2"> HTML5</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
      <Typography component="div" className={classes.chatBots}>
        <LogoHolder borderRadius="50px" />

        <Typography component="h2"> Conversational Bots</Typography>
        <Typography component="p">
          Aliquip exercitation laborum proident cupidatat et ad. Adipisicing
          reprehenderit ex ea magna magna aliqua anim. Sit dolore sunt officia
          ad consequat velit mollit proident exercitation proident aliqua do.
          Sunt aliquip exercitation mollit aute cillum dolor ea magna est fugiat
          sunt est. Dolore eu labore do occaecat veniam aliqua. Sunt eiusmod ex
          commodo occaecat aute minim nostrud quis.
        </Typography>
        <Typography component="footer">
          <Button> Notes</Button>
          <Button> Blog</Button>
          <Button> Docs</Button>
        </Typography>
      </Typography>
    </Typography>
  );
}
