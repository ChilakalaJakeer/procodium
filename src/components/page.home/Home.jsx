import React from "react";

import Intro from "./Intro";
import PortfolioSection from "./PortfolioSection";

export default function Home(props) {
  return (
    <div>
      <Intro />
      <PortfolioSection />
    </div>
  );
}
