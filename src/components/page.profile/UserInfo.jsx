import React from "react";

import { makeStyles } from "@material-ui/core/styles";

import { ProfilePicHolder } from "../elements/StyledComponents";
import { GRADIENT_INTRO } from "../../theme/globalStyles";

import GitHubIcon from "@material-ui/icons/GitHub";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import DescriptionIcon from "@material-ui/icons/Description";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  //overflow: "hidden",-> gives boxShadow bug
  root: {
    display: "grid",
    gridTemplateColumns: "1fr 10fr 1fr",
    gridTemplateRows: "200px auto", //1:2
    gridTemplateAreas: `"c   c   c"
    "b    b   b"`,
    boxShadow: theme.shadows[3],
    borderRadius: theme.shape.borderRadius,
    background: theme.palette.background.paper
  },
  coverPic: {
    gridArea: "c",
    background: GRADIENT_INTRO
  },
  body: {
    gridArea: "b",
    zIndex: theme.zIndex,
    display: "grid",
    gridTemplateColumns: "50px auto 50px",
    gridTemplateRows: `${"75px" /*(1/2)X EQUAL_SIDE_LENTH*/} 50px auto 75px`,
    gridTemplateAreas: `
    ".    .   ."
    ".    header   ."
    ".    summary   ."
    ".    footer  ."
    `,
    //smBelow
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "24px auto 24px",
      gridTemplateRows: `${"75px" /*(1/2)X EQUAL_SIDE_LENTH*/} 50px auto 75px`,
      gridTemplateAreas: `
    ".    .   ."
    ".    header   ."
    ".    summary  ."
    ".    footer  ."
    `
    },
    //xsBelow
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "16px auto 16px",
      gridTemplateRows: `${"75px" /*(1/2)X EQUAL_SIDE_LENTH*/} 50px auto 75px`,
      gridTemplateAreas: `
      ".    .   ."
    ".    header   ."
    ".    summary  ."
    ".    footer  ."
    `
    },
    gridRowGap: theme.spacing(2),
    "&>header": {
      gridArea: "header",
      placeSelf: "center",

      //flex
      "&>h2": {
        display: "flex",
        justifyContent: "center"
      }
    },

    "&>p": {
      gridArea: "summary",
      overflow: "hidden",
      textIndent: theme.spacing(10),
      maxHeight: theme.spacing(20)
    },

    "&>footer": {
      gridArea: "footer",
      alignSelf: "center",
      //flex
      display: "flex",
      flex: 1,
      justifyContent: "center",
      alignContent: "center",

      //social icons
      "&>div": {
        "&>*": {
          marginRight: theme.spacing(2),
          fontSize: theme.spacing(4)
        },

        "&>*:hover": {
          opacity: 0.5
        }
      }
    }
  }
}));

export default function UserInfo(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.coverPic}>
        <img className={classes.image} src="#" alt="" />
      </div>
      <div className={classes.body}>
        <ProfilePicHolder />

        <Typography component="header">
          <Typography component="h2" variant="h6">
            User Name
          </Typography>
          <Typography component="span" variant="subtitle1">
            caption&bull; caption
          </Typography>
        </Typography>

        <Typography component="p" variant="body1">
          <strong> Summary </strong> AliquiTypography quis qui labore eiusmod
          do. Adipisicing labore reprehenderit irure non ea ipsum id excepteur
          fugiat elit fugiat. Culpa commodo nulla consequat amet do in est
          labore. Aliquip quis qui labore eiusmod do. Adipisicing labore
          reprehenderit irure non ea ipsum id excepteur fugiat elit fugiat.
        </Typography>

        <Typography component="footer" variant="h6">
          <div>
            <FacebookIcon />
            <TwitterIcon />
            <InstagramIcon />
            <GitHubIcon />
            <DescriptionIcon />
          </div>
        </Typography>
      </div>
    </div>
  );
}
