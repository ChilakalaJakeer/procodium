import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import BlogCard from "../elements/BlogCard";

const useStyles = makeStyles(theme => ({
  root: {
    marginInline: theme.spacing(5),
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill,minmax(300px,1fr))",
    gridTemplateRows: "auto",
    gridGap: theme.spacing(3)
  }
}));
export default function UserBlogSection(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <BlogCard />
      <BlogCard />
      <BlogCard />
      <BlogCard />
      <BlogCard />
      <BlogCard />
      <BlogCard />
    </div>
  );
}
