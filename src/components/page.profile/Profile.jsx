import React from "react";

import { makeStyles } from "@material-ui/core/styles";

import UserInfo from "./UserInfo";
import UserBlogSection from "./UserBlogSection";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 6fr 1fr 1fr",
    gridTemplateRows: "auto",
    gridRowGap: theme.spacing(5),
    gridTemplateAreas: `".    .   userInfo    .   ."
    ".    userBlog   userBlog    userBlog   ."
    `,
    //smBelow
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "1fr  10fr 1fr",
      gridTemplateRows: "auto",
      gridRowGap: theme.spacing(5),
      gridTemplateAreas: `".      userInfo   ."
        ".       userBlog       ."
        `
    },
    //xsBelow
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "0.5fr  11fr 0.5fr",
      gridTemplateRows: "auto",
      gridRowGap: theme.spacing(5),
      gridTemplateAreas: `"userInfo    userInfo  userInfo"
        ".       userBlog       ."
        `
    }
  },
  userInfo: {
    gridArea: "userInfo"
  },
  userBlog: {
    gridArea: "userBlog"
  }
}));

export default function Profile(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.userInfo}>
        <UserInfo />
      </div>
      <div className={classes.userBlog}>
        <UserBlogSection />
      </div>
    </div>
  );
}
