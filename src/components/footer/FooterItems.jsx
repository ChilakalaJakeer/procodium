import React from "react";

import { NavItemsContainer, NavItem } from "../elements/StyledComponents";

export default function FooterItems(props) {
  const navNames = ["home", "blog", "about", "contact"];

  return (
    <NavItemsContainer>
      {navNames.map(navName => (
        <NavItem key={navName} display="block" margin="20px 0">
          {navName}
        </NavItem>
      ))}
    </NavItemsContainer>
  );
}
