import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";

import FooterItems from "./FooterItems";
import { GRADIENT_INTRO } from "../../theme/globalStyles";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridTemplateColumns: "repeat(5, 1fr)",
    gridTemplateRows: "1fr 1fr auto",
    gridTemplateAreas: `" .     n       .       .       ."
        " .     n       .       .       ."
        " c     c       c       c       c"`,
    gridGap: "20px",
    marginTop: "100px",
    background: GRADIENT_INTRO,
    //smBelow
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "repeat(5, 1fr)",
      gridTemplateRows: "1fr 1fr auto",
      gridTemplateAreas: `" .     n       .       .       ."
        " .     n       .       .       ."
        " c     c       c       c       c"`,
      gridGap: "20px",
      marginTop: "100px",
      background: GRADIENT_INTRO
    },
    //xsBelow
    [theme.breakpoints.down("xs")]: {
      gridTemplateColumns: "repeat(5, 1fr)",
      gridTemplateRows: "auto",
      gridTemplateAreas: `
          " c     c       c       c       c"`,
      marginTop: "100px",
      background: GRADIENT_INTRO
    }
  },
  navigation: {
    gridArea: "n"
  },

  footerBottom: {
    zIndex: "100px",
    gridArea: "c",
    height: "50px",
    background: "#0007",
    //flex
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  brandName: {
    flex: 0.4,
    justifySelf: "center",

    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  socialMedia: {
    flex: 0.4,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-evenly"
  },
  copyrights: {
    flex: 0.2,
    textAlign: "center"
  }
}));

export default function Footer(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Hidden xsDown>
        <div className={classes.navigation}>
          <FooterItems />
        </div>
      </Hidden>
      <div className={classes.footerBottom}>
        <span className={classes.brandName}>Procodium </span>
        <span className={classes.socialMedia}>
          <FacebookIcon />
          <TwitterIcon />
          <InstagramIcon />
        </span>
        <span className={classes.copyrights}>&copy; 2020</span>
      </div>
    </div>
  );
}
