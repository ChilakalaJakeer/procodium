import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { CardMedia, Avatar, Chip } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: "100%",
    maxHeight: "100%",
    //@theme-override
    boxShadow: "none",
    "&:hover h2": {
      background: `-webkit-linear-gradient( 150deg,${theme.palette.primary.main} 30%, ${theme.palette.secondary.main} 100%)`,
      WebkitBackgroundClip: "text",
      WebkitTextFillColor: "transparent"
    }
  },

  media: {
    height: "150px"
  },
  tags: {
    display: "flex",
    flexWrap: "wrap",
    "&>*": {
      marginRight: "8px",
      marginBottom: "8px",
      fontWeight: "700",
      "&:hover": {
        background: `-webkit-linear-gradient( 150deg,${theme.palette.primary.main} 30%, ${theme.palette.secondary.main} 100%)`,
        WebkitBackgroundClip: "text",
        WebkitTextFillColor: "transparent"
      }
    }
  },
  title: {
    lineHeight: "125%"
  },
  subtitle: {
    textIndent: "24px" //not working
  },
  articleInfo: {
    display: "grid",
    gridTemplateColumns: "1fr 6fr",
    gridTemplateRows: "auto",
    gridTemplateAreas: `"a   n   n"
    "a    s   s"`,
    gridColumnGap: "8px",
    alignItems: "center"
  },
  authorAvatar: {
    gridArea: "a"
  },
  authorName: {
    gridArea: "n"
  },
  articleStat: {
    gridArea: "s"
  }
}));

export default function BlogCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image="https://images.unsplash.com/photo-1583142305492-3938b117da28?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
        title="Media"
      />
      <CardContent className={classes.content}>
        <div className={classes.tags}>
          <Chip label="#tags" variant="outlined" onClick={() => null} />
          <Chip label="#square" variant="outlined" onClick={() => null} />
          <Chip label="#hexagon" variant="outlined" onClick={() => null} />
          <Chip label="#triangle" variant="outlined" onClick={() => null} />
        </div>
        <Typography
          variant="h6"
          component="h2"
          className={classes.title}
          gutterBottom
        >
          Sit do velit cillum aliqua proident dolor sit elit occaecat occaecat
          ullamco veniam anim.
        </Typography>
        <Typography
          variant="body2"
          component="p"
          className={classes.subtitle}
          gutterBottom
        >
          Aliquip et incididunt et cupidatat eiusmod sint esse ex tempor
          occaecat laboris elit est sunt. Dolor minim eiusmod Lorem Lorem
          reprehenderit enim labore nulla id adipisicing. Excepteur anim velit
          excepteur et duis exercitation in eiusmod tempor deserunt consequat
          irure adipisicing velit.
        </Typography>
        <div className={classes.articleInfo}>
          <Avatar className={classes.authorAvatar} />
          <Typography className={classes.authorName} component="h6">
            Author Name
          </Typography>
          <Typography className={classes.articleStat} color="textSecondary">
            3 Mar &bull; 10min read
          </Typography>
        </div>
      </CardContent>
    </Card>
  );
}
