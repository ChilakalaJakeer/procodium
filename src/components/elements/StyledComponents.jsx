import styled from "styled-components";
import { EQUAL_SIDE_LENTH, MARGIN } from "../../theme/globalStyles";

// Navigation
export const NavItemsContainer = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;
export const NavItem = styled.li`
  display: ${props => props.display};
  color: inherit;
  margin: ${props => props.margin};
  text-transform: uppercase;
  font-weight: 700;
`;

//Portfolio section

export const LogoHolder = styled.div`
  width: ${EQUAL_SIDE_LENTH};
  height: ${EQUAL_SIDE_LENTH};
  background: ${props => props.theme.palette.background.paper};

  -webkit-box-shadow: ${props =>
    props.theme
      .shadows[7]}; /* box-shadow from https://www.cssmatic.com/box-shadow */
  -moz-box-shadow: ${props => props.theme.shadows[7]};
  box-shadow: ${props => props.theme.shadows[7]};

  /* for turned square */
  transform: ${props => props.transform};

  /* for circle */
  border-radius: ${props => props.borderRadius};

  position: absolute;
  right: 50%;
  left: 50%;
  margin-left: ${MARGIN};
  margin-top: ${MARGIN};

  display: flex;
  flex: 1;
  place-content: center;
`;

export const HexagonLogoHolder = styled.div`
  width: ${EQUAL_SIDE_LENTH};
  height: ${EQUAL_SIDE_LENTH};
  position: absolute;
  right: 50%;
  left: 50%;
  margin-left: ${MARGIN};
  margin-top: ${MARGIN};
  /* http://brenna.github.io/csshexagon/ */
  .hexagon {
    position: relative;
    width: 150px;
    height: 86.6px;
    background: ${props => props.theme.palette.background.paper};
    margin: 43.3px 0;
    box-shadow: ${props => props.theme.shadows[7]};
  }

  .hexagon:before,
  .hexagon:after {
    content: "";
    position: absolute;
    z-index: 1;
    width: 106.07px;
    height: 106.07px;
    -webkit-transform: scaleY(0.5774) rotate(-45deg);
    -ms-transform: scaleY(0.5774) rotate(-45deg);
    transform: scaleY(0.5774) rotate(-45deg);
    background-color: inherit;
    left: 21.967px;
    box-shadow: ${props => props.theme.shadows[7]};
  }

  .hexagon:before {
    top: -53.033px;
  }

  .hexagon:after {
    bottom: -53.033px;
  }

  /*cover up extra shadows*/
  .hexagon span {
    display: block;
    position: absolute;
    top: 0px;
    left: 0;
    width: 150px;
    height: 86.6025px;
    z-index: 2;
    background: inherit;
  }

  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

//Profile pic holder
export const ProfilePicHolder = styled.div`
  width: ${EQUAL_SIDE_LENTH};
  height: ${EQUAL_SIDE_LENTH};
  background: ${props => props.theme.palette.background.paper};

  -webkit-box-shadow: ${props => props.theme.shadows[5]};
  -moz-box-shadow: ${props => props.theme.shadows[5]};
  box-shadow: ${props => props.theme.shadows[5]};
  border-radius: 10px;
  position: absolute;
  right: 50%;
  left: 50%;
  margin-left: ${MARGIN};
  margin-top: ${MARGIN};

  display: flex;
  flex: 1;
  place-content: center;
`;
