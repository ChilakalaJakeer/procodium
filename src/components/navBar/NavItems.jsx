import React from "react";
import Link from "next/link";

import { NavItemsContainer, NavItem } from "../elements/StyledComponents";

export default function NavItems(props) {
  const navNames = ["home", "blog", "about", "contact", "profile"];
  return (
    <NavItemsContainer>
      {navNames.map(navName => (
        <Link href={navName === "home" ? "/" : `/${navName}`}>
          <NavItem key={navName} display="inline-block" margin={`0px 20px`}>
            {/*margin inline ${theme.spacing(2.5)} */}

            {navName}
          </NavItem>
        </Link>
      ))}
    </NavItemsContainer>
  );
}
