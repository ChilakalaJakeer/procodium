import React from "react";
import Link from "next/link";

import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import MenuIcon from "@material-ui/icons/Menu";
import Slide from "@material-ui/core/Slide";

import { GRADIENT } from "../../theme/globalStyles";

const useStyles = makeStyles(theme => ({
  modalMenu: {
    background: GRADIENT,
    height: window.innerHeight,

    display: "grid",
    gridTemplateColumns: "repeat(6,2fr)",
    gridTemplateRows: "100px auto",
    gridTemplateAreas: `".    .   title    title   .    close"
    "nav    nav   nav    nav   nav    nav"
    `
  },

  title: {
    gridArea: "title",
    placeSelf: "center",
    color: theme.palette.primary.contrastText
  },
  close: {
    gridArea: "close",
    placeSelf: "center",
    color: theme.palette.primary.contrastText
  },
  navItemsContainer: {
    gridArea: "nav",
    padding: 0,
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),

    display: "flex",
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "space-around",
    alignContent: "center",
    textAlign: "center",
    listStyle: "none"
  },
  navItem: {
    textTransform: "uppercase",
    color: theme.palette.primary.contrastText
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

export default function NavMenu(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const navNames = ["home", "blog", "about", "contact", "profile"];
  return (
    <div>
      <IconButton onClick={() => setOpen(true)}>
        <MenuIcon style={{ color: "white" }} />
      </IconButton>
      <Dialog
        fullScreen
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <div className={classes.modalMenu}>
          <Typography variant="h5" className={classes.title}>
            <span onClick={() => setOpen(false)}>
              {"<Procodium/>" + window.innerWidth}
            </span>
          </Typography>
          <IconButton
            edge="start"
            color="inherit"
            onClick={() => setOpen(false)}
            aria-label="close"
            className={classes.close}
          >
            <CloseIcon />
          </IconButton>

          <ul className={classes.navItemsContainer}>
            {navNames.map(navName => (
              <Link href={navName === "home" ? "/" : `/${navName}`}>
                <Typography
                  component="li"
                  variant="h6"
                  key={navName}
                  display="block"
                  className={classes.navItem}
                  onClick={() => setOpen(false)}
                >
                  {navName}
                </Typography>
              </Link>
            ))}
          </ul>
        </div>
      </Dialog>
    </div>
  );
}
