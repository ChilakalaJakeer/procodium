import React from "react";
import Link from "next/link";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";

import NavItems from "./NavItems";
import Hidden from "@material-ui/core/Hidden";
import NavMenu from "./NavMenu";

const useStyles = makeStyles(theme => ({
  appBar: {},
  toolbar: {
    display: "flex",
    flex: 1,
    alignContent: "center"
  },
  brandName: {
    flex: 1,
    justifySelf: "flex-start"
  },
  navItems: {
    flex: 3,
    justifySelf: "flex-end"
  }
}));

export default function NavBar(props) {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="absolute">
        <Toolbar className={classes.toolbar}>
          <Link href="/">
            <span className={classes.brandName}>Procodium</span>
          </Link>
          <Hidden smDown>
            <NavItems className={classes.navItems} />
          </Hidden>
          <Hidden mdUp>
            <NavMenu />
          </Hidden>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </div>
  );
}
