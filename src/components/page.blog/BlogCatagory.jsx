import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import BlogCard from "../elements/BlogCard";

const useStyles = makeStyles(theme => ({
  section: {
    background: "transparent",
    boxShadow: "none"
  },
  heading: {
    textTransform: "uppercase",
    // textAlign: "center",
    width: "100%",
    fontWeight: "900",
    // fontFamily: "Fira Code",
    "&:hover": {
      background: `-webkit-linear-gradient( 150deg,${theme.palette.primary.main} 10%, ${theme.palette.secondary.main} 100%)`,
      WebkitBackgroundClip: "text",
      WebkitTextFillColor: "transparent"
    }
  },
  articles: {
    marginInline: theme.spacing(5),
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill,minmax(300px,1fr))",
    gridTemplateRows: "auto",
    gridGap: theme.spacing(3)
  }
}));
export default function BlogCatagory(props) {
  const [open, setOpen] = useState(true);
  const classes = useStyles();
  const { catogaryName, articles } = props;

  return (
    <ExpansionPanel
      className={classes.section}
      expanded={open}
      onChange={() => setOpen(!open)}
    >
      <ExpansionPanelSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
        expandIcon={<ExpandMoreIcon />}
      >
        <Typography className={classes.heading}>{catogaryName}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div className={classes.articles}>
          {/* TODO
        Here comes data to render  */}
          {articles.map(elem => (
            <BlogCard key={elem} />
          ))}
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}
