import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";

import BlogCatogary from "./BlogCatagory";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridTemplateColumns: "1fr 8fr 1fr",
    gridTemplateRows: "auto",
    gridTemplateAreas: `".   b   ."`,
    "&>*": {
      gridArea: "b",
      "&>*": {
        background: "transparent",
        boxShadow: "none"
      }
    }
  }
}));

export default function Blog(props) {
  const classes = useStyles();

  const tempArr = [
    {
      catogaryName: "web technologies",
      articles: [1, 2, 3],
      id: 1
    },
    {
      catogaryName: "Machine Learning",
      articles: [1, 3, 5, 7, 8, 9],
      id: 2
    },
    { catogaryName: "Misc", articles: [2, 9, 7, 6], id: 2 }
  ];
  return (
    <div className={classes.root}>
      <div>
        {tempArr.map(elem => (
          <BlogCatogary
            key={elem.id}
            catogaryName={elem.catogaryName}
            articles={elem.articles}
          />
        ))}
      </div>
    </div>
  );
}
